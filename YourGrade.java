import java.util.Scanner;

public class YourGrade {

    public static String calculateGrade(int score) {
        if (score >= 80) {
            return "A";
        } else if (score >= 75) {
            return "B+";
        } else if (score >= 70) {
            return "B";
        } else if (score >= 65) {
            return "C+";
        } else if (score >= 60) {
            return "C";
        } else if (score >= 55) {
            return "D+";
        } else if (score >= 50) {
            return "D";
        } else {
            return "F";
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Put score (0-100): ");
        int score = scanner.nextInt();

        if (score >= 0 && score <= 100) {
            String grade = calculateGrade(score);
            System.out.println("Score " + score + " = Grade " + grade);
        } else {
            System.out.println("Error");
        }
    }
}