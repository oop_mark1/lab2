import java.util.Scanner;

public class javaSwitch08 {
    public static void main(String[] args) {
        int num;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number;");
        num = sc.nextInt();
        switch (num) {
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Three");

            default:
                System.out.println("Unknown");
        }
    }

}
